﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using GSB_CR.model.classes;
using GSB_CR.model.DAL;

namespace GSB_CR.model.DAO
{
    class QuestionnaireDAO : DeclarationsDAO
    {
        public bool InsertQuestionnaire(Questionnaire questionnaire) 
        {
            bool verif;
            try
            {
                connection.Open();

                MySqlCommand cmd = this.connection.CreateCommand();

                //requete(s)
                cmd.CommandText = "INSERT INTO Questionnaire(libelle)"
                                     + " VALUES(@Nom_quest);";

                cmd.Prepare();

                cmd.Parameters.AddWithValue("@Nom_quest", questionnaire.NOM);

                cmd.ExecuteNonQuery();



                this.connection.Close();

                verif = true;
            }
            catch (Exception)
            {
                verif = false;
            }
            return verif;
        }
    }
}
