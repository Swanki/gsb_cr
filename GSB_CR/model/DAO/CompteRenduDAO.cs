using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using GSB_CR.model.classes;
using GSB_CR.model.DAL;

namespace GSB_CR.model.DAO
{
    public class CompteRenduDAO : DeclarationsDAO
    {
		public List<CompteRendu> SelectCompteRendus()
		{
			List<CompteRendu> compteRendus = new List<CompteRendu>();
			try
			{

				connection.Open();
                /*
				MySqlCommand cmd = this.connection.CreateCommand();

				//requ�te(s)
				cmd.CommandText = "SELECT COL_MATRICULE,RAP_NUM,PRA_NOM,PRA_PRENOM,RAP_DATE,RAP_BILAN,RAP_MOTIF " +
				"FROM COMPTE_RENDU " +
				"INNER JOIN PRATICIEN " +
				"ON PRATICIEN.PRA_NUM = COMPTE_RENDU.PRA_NUM;";

				MySqlDataReader reader = cmd.ExecuteReader();
				while (reader.Read())
				{
					string c = reader.GetString("COL_MATRICULE");
					int r = reader.GetInt32("RAP_NUM");
					string n = reader.GetString("PRA_NOM");
					string p = reader.GetString("PRA_PRENOM");
					DateTime ra= reader.GetDateTime("RAP_DATE");
					string b = reader.GetString("RAP_BILAN");
					string m = reader.GetString("RAP_MOTIF");

					Praticien praticien = new Praticien(n, p);
					CompteRendu compteRendu = new CompteRendu(c,r,ra,b,m, praticien);

					compteRendus.Add(compteRendu);
				}
                */
				connection.Close();

				
			}
			catch (Exception)
			{
				 throw new Exception("erreur de connection à la base de données");
			}
			return compteRendus;
		}

        public bool ModifierCompteRendu(CompteRendu compteRendu)
        {
            bool verif;
            try
            {
                connection.Open();

                MySqlCommand cmd = this.connection.CreateCommand();

                //requete(s)
                cmd.CommandText = " UPDATE COMPTE_RENDU SET COL_MATRICULE=@col_mat, RAP_DATE=@rap_date, PRA_NUM=@pra_num, BILAN=@rap_bilan, RAP_MOTIF=@rap_motif"
                                     + " WHERE RAP_NUM=@rap_num";

                cmd.Prepare();

				cmd.Parameters.AddWithValue("@col_mat", compteRendu.COL_MATRICULE);
				cmd.Parameters.AddWithValue("@rap_num", compteRendu.RAP_NUM);
                cmd.Parameters.AddWithValue("@pra_num", compteRendu.PRA_NUM);
                cmd.Parameters.AddWithValue("@rap_date", compteRendu.RAP_DATE); 
                cmd.Parameters.AddWithValue("@rap_bilan", compteRendu.RAP_BILAN); 
                cmd.Parameters.AddWithValue("@rap_motif", compteRendu.RAP_MOTIF);               

                cmd.ExecuteNonQuery();

                this.connection.Close();

                verif = true;
            }
            catch (Exception)
            {
                verif = false;
            }
            return verif;
        }

         public bool InsertCompteRendu(CompteRendu compteRendu)
        {
            bool verif;
            try
            {
                connection.Open();

                MySqlCommand cmd = this.connection.CreateCommand();

                //requete(s)
                cmd.CommandText = "INSERT INTO COMPTE_RENDU(COL_MATRICULE, RAP_NUM, PRA_NUM, RAP_DATE, RAP_BILAN, RAP_MOTIF)"
                                     + " VALUES(@col_ma,@rap_num,@pra_num,@rap_date,@rap_bilan,@rap_motif);";

                cmd.Prepare();

                cmd.Parameters.AddWithValue("@col_ma", compteRendu.COL_MATRICULE);
                cmd.Parameters.AddWithValue("@rap_num", compteRendu.RAP_NUM);
                cmd.Parameters.AddWithValue("@pra_num", compteRendu.PRA_NUM);
                cmd.Parameters.AddWithValue("@rap_date", compteRendu.RAP_DATE); 
                cmd.Parameters.AddWithValue("@rap_bilan", compteRendu.RAP_BILAN); 
                cmd.Parameters.AddWithValue("@rap_motif", compteRendu.RAP_MOTIF);               

                cmd.ExecuteNonQuery();

                this.connection.Close();
                
                verif = true;
            }
            catch (Exception)
            {
                verif = false;
            }
            return verif;
        }
    }
}