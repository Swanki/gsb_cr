﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSB_CR.model.classes
{
    public class CompteRendu
    {
        public string COL_MATRICULE;
        public decimal RAP_NUM;
        public int PRA_NUM;
        public DateTime RAP_DATE;
        public string RAP_BILAN;
        public string RAP_MOTIF;
		public Praticien Praticien;

        public CompteRendu()
        {
           
        }

		public CompteRendu(string Col_matricule, decimal Rap_num, int Pra_num, DateTime Rap_date, string Rap_blian, string Rap_motif)
		{
			COL_MATRICULE = Col_matricule;
			RAP_NUM = Rap_num;
			PRA_NUM = Pra_num;
			RAP_DATE = Rap_date;
			RAP_BILAN = Rap_blian;
			RAP_MOTIF = Rap_motif;
			
		}

		public CompteRendu(string Col_matricule, decimal Rap_num, DateTime Rap_date, string Rap_blian, string Rap_motif, Praticien praticien)
        {
            COL_MATRICULE = Col_matricule;
            RAP_NUM = Rap_num;
            RAP_DATE = Rap_date;
            RAP_BILAN = Rap_blian;
            RAP_MOTIF = Rap_motif;
			Praticien = praticien;
        }

        public override string ToString()
        {
            return "Rapport numéro : " + RAP_NUM + "  /  " + Praticien + "  |  " + RAP_DATE;
        }
    }
}
