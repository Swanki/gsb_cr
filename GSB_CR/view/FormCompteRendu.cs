﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using GSB_CR.model.classes;
using GSB_CR.model.DAO;

namespace GSB_CR.view
{
    public partial class Compte_Rendu : Form
    {
        
        public Compte_Rendu()
        {
            InitializeComponent();
        }

        private void Compte_Rendu_Load(object sender, EventArgs e)
        {
			CompteRenduDAO compteRenduDAO = new CompteRenduDAO();
			CompteRendu compteRendu = new CompteRendu();

			try
			{
				lstCompteRendu.DataSource = compteRenduDAO.SelectCompteRendus();
			}
			catch
			{
				lstCompteRendu.DataSource = null;
				MessageBox.Show("erreur de connection à la base de données ");
			}
		}

        private void lstCompteRendu_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

		private void edit_cr_Click(object sender, EventArgs e)
		{
			EditCR editCR = new EditCR();
			editCR.Show();
		}

		private void btn_modif_Click(object sender, EventArgs e)
		{
			//CompteRendu compteRenduSelected = (CompteRendu)lstCompteRendu.SelectedItem;
			
			//EditCR editCR = new EditCR();
			//editCR.compteRendu = compteRenduSelected;
			//editCR.Show();

		}

		private void lstCompteRendu_SelectedIndexChanged_1(object sender, EventArgs e)
		{

		}

        private void btn_questionnaire_Click(object sender, EventArgs e)
        {

        }

        private void btn_creationQuestionnaire_Click(object sender, EventArgs e)
        {
            CréationQuestionnaire questionnaireCR = new CréationQuestionnaire();
            questionnaireCR.Show();
        }
    }
}
