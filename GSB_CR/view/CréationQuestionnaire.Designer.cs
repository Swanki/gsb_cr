﻿namespace GSB_CR.view
{
    partial class CréationQuestionnaire
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_Quest = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.txt_Q1 = new System.Windows.Forms.TextBox();
            this.txt_Q2 = new System.Windows.Forms.TextBox();
            this.txt_Q3 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txt_Quest
            // 
            this.txt_Quest.Location = new System.Drawing.Point(134, 12);
            this.txt_Quest.Name = "txt_Quest";
            this.txt_Quest.Size = new System.Drawing.Size(331, 20);
            this.txt_Quest.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nom du questionnaire :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Q1 :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Q2 :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 108);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Q3 :";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(15, 146);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(450, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "Céer";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txt_Q1
            // 
            this.txt_Q1.Location = new System.Drawing.Point(45, 59);
            this.txt_Q1.Name = "txt_Q1";
            this.txt_Q1.Size = new System.Drawing.Size(420, 20);
            this.txt_Q1.TabIndex = 6;
            // 
            // txt_Q2
            // 
            this.txt_Q2.Location = new System.Drawing.Point(45, 82);
            this.txt_Q2.Name = "txt_Q2";
            this.txt_Q2.Size = new System.Drawing.Size(420, 20);
            this.txt_Q2.TabIndex = 7;
            // 
            // txt_Q3
            // 
            this.txt_Q3.Location = new System.Drawing.Point(45, 105);
            this.txt_Q3.Name = "txt_Q3";
            this.txt_Q3.Size = new System.Drawing.Size(420, 20);
            this.txt_Q3.TabIndex = 8;
            // 
            // CréationQuestionnaire
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(482, 186);
            this.Controls.Add(this.txt_Q3);
            this.Controls.Add(this.txt_Q2);
            this.Controls.Add(this.txt_Q1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txt_Quest);
            this.Name = "CréationQuestionnaire";
            this.Text = "CréationQuestionnaire";
            this.Load += new System.EventHandler(this.CréationQuestionnaire_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_Quest;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txt_Q1;
        private System.Windows.Forms.TextBox txt_Q2;
        private System.Windows.Forms.TextBox txt_Q3;
    }
}