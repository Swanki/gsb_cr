﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GSB_CR.model.classes;
using GSB_CR.model.DAO;

namespace GSB_CR.view
{
    public partial class CréationQuestionnaire : Form
    {
        public CréationQuestionnaire()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            QuestionnaireDAO questionnaireDAO = new QuestionnaireDAO();
            Questionnaire questionnaire = new Questionnaire();
            List<Question> questions = new List<Question>();
            Question question1 = new Question(txt_Q1.Text);
            Question question2 = new Question(txt_Q2.Text);
            Question question3 = new Question(txt_Q3.Text);

            questionnaire.NOM = txt_Quest.Text;

            questions.Add(question1);
            questions.Add(question2);
            questions.Add(question3);

            questionnaireDAO.InsertQuestionnaire(questionnaire);
        }

        private void CréationQuestionnaire_Load(object sender, EventArgs e)
        {

        }
    }
}
