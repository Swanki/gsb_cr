﻿namespace GSB_CR.view
{
    partial class Compte_Rendu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstCompteRendu = new System.Windows.Forms.ListBox();
            this.edit_cr = new System.Windows.Forms.Button();
            this.btn_modif = new System.Windows.Forms.Button();
            this.btn_creationQuestionnaire = new System.Windows.Forms.Button();
            this.btn_questionnaire = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lstCompteRendu
            // 
            this.lstCompteRendu.FormattingEnabled = true;
            this.lstCompteRendu.Location = new System.Drawing.Point(12, 12);
            this.lstCompteRendu.Name = "lstCompteRendu";
            this.lstCompteRendu.Size = new System.Drawing.Size(358, 420);
            this.lstCompteRendu.TabIndex = 0;
            this.lstCompteRendu.SelectedIndexChanged += new System.EventHandler(this.lstCompteRendu_SelectedIndexChanged_1);
            // 
            // edit_cr
            // 
            this.edit_cr.Location = new System.Drawing.Point(477, 12);
            this.edit_cr.Name = "edit_cr";
            this.edit_cr.Size = new System.Drawing.Size(113, 42);
            this.edit_cr.TabIndex = 1;
            this.edit_cr.Text = "éditer un compte rendu";
            this.edit_cr.UseVisualStyleBackColor = true;
            this.edit_cr.Click += new System.EventHandler(this.edit_cr_Click);
            // 
            // btn_modif
            // 
            this.btn_modif.Location = new System.Drawing.Point(477, 88);
            this.btn_modif.Name = "btn_modif";
            this.btn_modif.Size = new System.Drawing.Size(113, 47);
            this.btn_modif.TabIndex = 2;
            this.btn_modif.Text = "Modifier le CompteRendu selectionné";
            this.btn_modif.UseVisualStyleBackColor = true;
            this.btn_modif.Click += new System.EventHandler(this.btn_modif_Click);
            // 
            // btn_creationQuestionnaire
            // 
            this.btn_creationQuestionnaire.Location = new System.Drawing.Point(477, 342);
            this.btn_creationQuestionnaire.Name = "btn_creationQuestionnaire";
            this.btn_creationQuestionnaire.Size = new System.Drawing.Size(112, 47);
            this.btn_creationQuestionnaire.TabIndex = 3;
            this.btn_creationQuestionnaire.Text = "Creation questionnaire";
            this.btn_creationQuestionnaire.UseVisualStyleBackColor = true;
            this.btn_creationQuestionnaire.Click += new System.EventHandler(this.btn_creationQuestionnaire_Click);
            // 
            // btn_questionnaire
            // 
            this.btn_questionnaire.Location = new System.Drawing.Point(477, 289);
            this.btn_questionnaire.Name = "btn_questionnaire";
            this.btn_questionnaire.Size = new System.Drawing.Size(112, 47);
            this.btn_questionnaire.TabIndex = 4;
            this.btn_questionnaire.Text = "Questionnaire";
            this.btn_questionnaire.UseVisualStyleBackColor = true;
            this.btn_questionnaire.Click += new System.EventHandler(this.btn_questionnaire_Click);
            // 
            // Compte_Rendu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(602, 440);
            this.Controls.Add(this.btn_questionnaire);
            this.Controls.Add(this.btn_creationQuestionnaire);
            this.Controls.Add(this.btn_modif);
            this.Controls.Add(this.edit_cr);
            this.Controls.Add(this.lstCompteRendu);
            this.Name = "Compte_Rendu";
            this.Text = "Compte_Rendu";
            this.Load += new System.EventHandler(this.Compte_Rendu_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lstCompteRendu;
        private System.Windows.Forms.Button edit_cr;
		private System.Windows.Forms.Button btn_modif;
        private System.Windows.Forms.Button btn_creationQuestionnaire;
        private System.Windows.Forms.Button btn_questionnaire;
    }
}